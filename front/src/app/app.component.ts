import { Component, OnDestroy, OnInit } from '@angular/core';
import { Jobs } from './Models/jobs.model';
import { ApiService } from './Service/api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent  implements OnInit , OnDestroy{

  constructor( private api: ApiService){

  }
  ngOnDestroy(): void {
    this.api.getAllJobs(this.getEndUrl)
  }
  title = 'front';
  jobs: Jobs[];
  getEndUrl: string = 'api/jobs';
  saveEndUrl: string = 'api/jobs/all'
  saveOneEndUrl : string = 'api/jobs/one'
  cols: any[];
  loading: boolean = true;

  boutonLoading: boolean = false;


  ngOnInit(): void {
    this.getAllJobs();

    this.cols = [
      { field: 'id', header: 'Id', width: '10%' },
      { field: 'status', header: 'Status', width: '10%'   },
      { field: 'name', header: 'Name', width: '10%'  },
      { field: 'commit', header: 'Commit', width: '40%'  },
      { field: 'pipeline', header: 'Pipeline', width: '15%'  },
      { field: 'pipelineStatus', header: 'PipelineStatus', width: '15%'  },
  ];
  }


  getAllJobs() {
    this.loading = true;
    //this.boutonLoading = true;
    this.api.getAllJobs(this.getEndUrl).subscribe(
      response => {
        this.jobs = response.sort((first, second ) => second.id - first.id );
        this.loading = false;
        console.log(this.jobs);

      }, (err) => {
        console.log(err)
      }
    )
  }



  saveAllJobs() {
    this.loading = true;
    this.boutonLoading = true;
    this.api.saveAllJobs(this.saveEndUrl).subscribe(
      response => {
        console.log(response);
        this.getAllJobs();
        this.boutonLoading = false;
        this.loading = false;

      }, (err) => {
        console.log(err)
      }
    )
  }



  saveOneJobs() {
    this.loading = true;
    this.boutonLoading = true;
    this.api.saveOneJobs(this.saveOneEndUrl).subscribe(
      response => {
        console.log(response);
        this.getAllJobs();
        this.boutonLoading = false;
        this.loading = false;
      }, (err) => {
        console.log(err)
      }
    )
  }
}
