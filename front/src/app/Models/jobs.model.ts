export class Jobs {
     id: number;
	status: string ;
	stage: string ;
	name: string;
	commit : string ;
    pipeline : number;
	pipelineStatus: string;
}