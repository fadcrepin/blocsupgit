import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { UtilsService } from './utils.service';
import { retry, catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpBackend } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  
  constructor(
    private router: Router,
    private utilsService: UtilsService,
    private httpClient: HttpClient) { }

    private baseURL = this.utilsService.getApiUrl();
    httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
    })
  }

  getAllJobs(endUrl: string): Observable<any> {
    
    return this.httpClient.get<any>(this.baseURL + endUrl, this.httpOptions)
      .pipe(
        retry(3),
        catchError(this.errorHandl)
      )
  }

  saveAllJobs(endUrl: string): Observable<any> {
    
    return this.httpClient.post<any>(this.baseURL + endUrl, this.httpOptions)
      .pipe(
        retry(3),
        catchError(this.errorHandl)
      )
  }


  saveOneJobs(endUrl: string): Observable<any> {
    
    return this.httpClient.post<any>(this.baseURL + endUrl, this.httpOptions)
      .pipe(
        retry(3),
        catchError(this.errorHandl)
      )
  }

    errorHandl(error) {
      let errorMessage = '';
      let errorStatus = '';
      if (error.error instanceof ErrorEvent) {
        // Get client-side error
        errorMessage = error.error.message;
      } else {
        // Get server-side error
        errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        errorStatus = error.status;
      }
      console.log(errorMessage);
      return throwError(errorStatus);
    }
}
