import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  
  constructor(
    private myRouter: Router) { }

  _getRawConfig() {
    return window['__WT_APP_CONFIG__'];
  }

  getApiUrl() {
    return this._getRawConfig().apiBaseUrl;
  }

  routeNavigate(path: string) {
    this.myRouter.navigateByUrl('/' + path);
  }
}
