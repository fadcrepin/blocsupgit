const request = require('request');
const express = require('express');
const cron = require('node-cron');
const bodyParser = require('body-parser');
const apiHelper = require('./helpers/apiHelpers');
var cors = require('cors')
const Web3 = require('web3');
const web3 = new Web3('ws://localhost:7545');
const JobContract = require('../../blockchain/src/abis/Projetpro.json');

const app = express();
app.use(cors());
//const PROJECT_TOKEN = 'gTBsdjZCrpxRVL8sRnzK';
//const JOB_URL = 'https://gitlab.com/api/v4/projects/22288816/jobs'
const JOB_URL = 'https://gitlab.com/api/v4/projects/26688884/jobs'
const PROJECT_TOKEN = 'n_vMXaSEhzwut1iBEUgt'

app.use(async function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');

    next();
});

app.use(bodyParser.json());

// app.post('/api/jobs', async function(req, res, next) {
//     console.log(req.body);
//     res.status(201).json({
//         message: 'Objet créé !'
//     });
// });
/*
cron.schedule('* * * * * *', function() {
  console.log('running a task every seconds');
  fetchJobsAndPersistOneJobSpecial( JOB_URL, res);
});
*/

app.get('/api/jobs', async function(req, res, next) {
    const networkId = await web3.eth.net.getId().catch(() => res.status(500).json({msg: 'cannot access to networkId'}));
    const networkData = JobContract.networks[networkId];
   // console.log("networkId", networkId);
   // console.log("networkData", networkData);
    if (networkData) {
        const jobContract = new web3.eth.Contract(JobContract.abi, networkData.address);
        console.log("networkData.address",networkData.address);
        //writeLog("fetching all pipelines");
        jobContract
            .getPastEvents('JobCreated', {fromBlock: 0, toBlock: 'latest'})
            .then(events => {
                // filtrage des données de l'event pour ne garder que les données des jobs
                //writeLog(events.length + " jobs");
                console.log(events.length);
                const jobs = events.map(event => fromEvent(event));
                res.status(200).send(jobs);
            })
            .catch(err => res.status(500).json({error: JSON.stringify(err)}));
    }
});

app.post('/api/jobs/one', async function(req, res, next) {

    fetchJobsAndPersistOneJob( JOB_URL, res);
});
app.post('/api/jobs/all', async function (req, res) {

    fetchJobsAndPersist( JOB_URL, res);
});



estimateGasPromise = (contract, job) => {
    return contract
        .methods
        .createJob( job.status, job.stage, job.name, job.commit, job.pipeline, job.pipelineStatus, job.created_at)
        .estimateGas()
}

sendJob = async function (contract, account, job, gas) {
    return contract
        .methods
        .createJob( job.status, job.stage, job.name, job.commit, job.pipeline, job.pipelineStatus, job.created_at)
        .send({from: account, gas: gas})
}

fromEvent = jobCreatedEvent => ({
    id: jobCreatedEvent.returnValues.id,
    status: jobCreatedEvent.returnValues.status,
    //branch: jobCreatedEvent.returnValues.branch,
    stage: jobCreatedEvent.returnValues.stage,
    name: jobCreatedEvent.returnValues.name,
    commit: jobCreatedEvent.returnValues.commit,
    pipeline: jobCreatedEvent.returnValues.pipeline,
    pipelineStatus: jobCreatedEvent.returnValues.pipelineStatus,
    created_at:jobCreatedEvent.returnValues.commit.created_at
});

fromGitlab = apiJobs => ({
   // id: apiJobs.id || '',
    status: apiJobs.status || '',
    //branch: apiJobs.ref || '',
    stage: apiJobs.stage || '',
    name: apiJobs.name || '',
    commit: apiJobs.commit.id || '',
    pipeline: apiJobs.pipeline.id || '',
    pipelineStatus: apiJobs.pipeline.status || '',
    created_at: apiJobs.commit.created_at || ''
});


fetchJobsAndPersist = (address, res) => {
    request({
        url: address,
        headers: {'Authorization': 'Bearer ' + PROJECT_TOKEN},
        rejectUnauthorized: false
    }, async function (error, response) {
        if (!error) {
            const jobs = JSON.parse(response.body);
            const accounts = await web3.eth.getAccounts().catch(() => res.status(500).json({msg: 'error while getting accounts'}));
            const networkId = await web3.eth.net.getId();
            const networkData = JobContract.networks[networkId];
            console.log(jobs);
            console.log(accounts);
            console.log(networkId);
            console.log(networkData);

            if (networkData && jobs && jobs.length > 0) {
                const jobContract = new web3.eth.Contract(JobContract.abi, networkData.address);
                const jobCreateds = [];
                jobs
                    .map(job => fromGitlab(job))
                    .forEach(job =>
                        estimateGasPromise(jobContract, job)
                            .then(gas =>
                                sendJob(jobContract, accounts[0], job, gas)
                                    .then(onResolved => {
                                        jobCreateds.push(onResolved.events.JobCreated.returnValues);
                                        if (jobCreateds.length === jobs.length) {
                                          console.log(jobCreateds);
                                            res.status(201).send(jobCreateds);
                                        }
                                    }))
                            .catch(err => res.status(500).json(err))
                    );
            }
        }
    });
}


fetchJobsAndPersistOneJob = (address, res) => {
  request({
      url: address,
      headers: {'Authorization': 'Bearer ' + PROJECT_TOKEN},
      rejectUnauthorized: false
  }, async function (error, response) {
        if (!error) {
            var lastjobs;
            const jobs = JSON.parse(response.body);
            const accounts = await web3.eth.getAccounts().catch(() => res.status(500).json({msg: 'error while getting accounts'}));
            const networkId = await web3.eth.net.getId();
            const networkData = JobContract.networks[networkId];
            lastjobs = jobs[0];
            //lastjobs = jobs[jobs.length -1];
            //console.log(jobs);
            console.log(accounts);
            //console.log(networkId);
            console.log(networkData);
            //console.log(lastjobs);
            console.log(lastjobs);
            if (networkData && lastjobs) {
                const jobContract = new web3.eth.Contract(JobContract.abi, networkData.address);
                const jobCreateds = [];
                const job = fromGitlab(lastjobs);
                console.log('job');
                estimateGasPromise(jobContract, job)
                    .then(gas =>
                        sendJob(jobContract, accounts[0], job, gas)
                            .then(onResolved => {
                                jobCreateds.push(onResolved.events.JobCreated.returnValues);
                                  console.log('jobCreateds');
                                    res.status(201).send(jobCreateds);

                            }))
                    .catch(err => res.status(500).json(err));
            }
        }
    });
}

getJobsList = (address) => {
  request({
      url: address,
      headers: {'Authorization': 'Bearer ' + PROJECT_TOKEN},
      rejectUnauthorized: false
  }, async function (error, response) {
        if (!error) {
            var lastjobs;
            const jobs = JSON.parse(response.body);
            const accounts = await web3.eth.getAccounts().catch(() => res.status(500).json({msg: 'error while getting accounts'}));
            const networkId = await web3.eth.net.getId();
            const networkData = JobContract.networks[networkId];
            const number = jobs.length;
            //console.log(number);
            //lastjobs = jobs[jobs.length -1];
            return number;
        }
    });

}

fetchJobsAndPersistOneJobSpecial = (address, res) => {
  request({
      url: address,
      headers: {'Authorization': 'Bearer ' + PROJECT_TOKEN},
      rejectUnauthorized: false
  }, async function (error, response) {
        if (!error) {
            var lastjobs;
            const jobs = JSON.parse(response.body);
            const accounts = await web3.eth.getAccounts().catch(() => res.status(500).json({msg: 'error while getting accounts'}));
            const networkId = await web3.eth.net.getId();
            const numberBlock = await web3.eth.net.getPeerCount().catch(() => res.status(500).json({msg: 'cannot access to networkId'}));
            const networkData = JobContract.networks[networkId];
            const numberJobs = jobs.length;
            lastjobs = jobs[numberJobs-numberBlock];
            //console.log(jobs);
            console.log(accounts);
            //console.log(networkId);
            console.log(networkData);
            //console.log(lastjobs);
            console.log(lastjobs);
            if (networkData && lastjobs && numberBlock<=numberJobs) {
                const jobContract = new web3.eth.Contract(JobContract.abi, networkData.address);
                const jobCreateds = [];
                const job = fromGitlab(lastjobs);
                estimateGasPromise(jobContract, job)
                    .then(gas =>
                        sendJob(jobContract, accounts[0], job, gas)
                            .then(onResolved => {
                                jobCreateds.push(onResolved.events.JobCreated.returnValues);
                                    res.status(201).send(jobCreateds);

                            }))
                    .catch(err => res.status(500).json(err));
            }
        }
    });
}

getJobsList = (address) => {
  request({
      url: address,
      headers: {'Authorization': 'Bearer ' + PROJECT_TOKEN},
      rejectUnauthorized: false
  }, async function (error, response) {
        if (!error) {
            var lastjobs;
            const jobs = JSON.parse(response.body);
            const accounts = await web3.eth.getAccounts().catch(() => res.status(500).json({msg: 'error while getting accounts'}));
            const networkId = await web3.eth.net.getId();
            const networkData = JobContract.networks[networkId];
            const number = jobs.length;
            //console.log(number);
            //lastjobs = jobs[jobs.length -1];
            return number;
        }
    });

}

function sizes (name) {
var abi = name;
var size = (abi.bytecode.length / 2) - 1 ;
var deployedSize = (abi.deployedBytecode.length / 2) - 1 ;
return {name, size, deployedSize} ;
}


module.exports = app;
