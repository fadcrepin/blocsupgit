pragma solidity >=0.4.21 <0.6.0;

contract Projetpro{
	string public name;
	uint public jobCount = 0; //number of product in the blockchain

	mapping(uint=> Displayjob) public displayjobs;

	struct Displayjob{
	uint id;
	string status;
	string stage;
	string name;
	//string username;
	string commit;
	uint pipeline;
	string pipelineStatus;
	//String stage;
	string created_at;
	}


//create event
event JobCreated(
	uint id,
	string status,
	string stage,
	string name,
	//string username;
	string commit,
	uint pipeline,
	string pipelineStatus,
	//String stage;
	string created_at
	);

constructor() public{
name = "apprentissage Blockchain Projetpro";
}



function createJob(string memory _status,string memory _stage,string memory _name,string memory _commit,uint _pipeline,string memory _pipelineStatus,string memory _created_at) public {
// Require a valid name
//require(bytes(_name).length > 0);

//Require a valid price
//require(_price > 0);

//Increment job count
jobCount++;

// create the job in blockchain
displayjobs[jobCount] = Displayjob(jobCount,_status,_stage,_name,_commit,_pipeline, _pipelineStatus,_created_at);

//Trigger an event
emit JobCreated(jobCount,_status,_stage,_name,_commit,_pipeline, _pipelineStatus,_created_at);
}
}
