import React, { Component } from 'react';
import Projetpro from '../abis/Projetpro.json';

class Main extends Component {

  render() {
    return (

        <div id ="content">
      
        <p>&nbsp;</p>
        <h2>Logs Tracking List</h2>
        <table className="table">
        <thead>
        <tr>
        <th scope="col">#</th>
        <th scope="col">Status</th>
        <th scope="col">Pipeline</th>
        <th scope="col">name</th>
         <th scope="col">Commit</th>
         <th scope="col">stages</th>
        <th scope="col"></th>
        </tr>
       </thead> 

       <tbody id="productlist"> 
       <tr>
       <th scope="row">1</th>
       <td>Passed</td>
       <td>#2345</td>
       <td>Amandine</td>
       <td> 1 commit</td>
       <td>Deploy</td>
       <td><button>Detail</button></td>
       </tr>

       <tr>
       <th scope="row">2</th>
       <td>Passed</td>
       <td>#2346</td>
       <td>Bibon</td>
       <td> 2 commit</td>
       <td>Prod</td>
       <td><button>Detail</button></td>
       </tr>



       </tbody>
       </table>
       
        </div>
    );
  }
}

export default Main;